def collapse(title)
  <<EOS
<div class="collapse" markdown="0">
  <input type="checkbox" id="#{title.object_id}">
  <h2>
    <label for="#{title.object_id}">
      #{ title }
    </label>
  </h2>
  <div class="collapse_inner" markdown="1">
EOS
end

def collapse_end
  <<EOS
  </div>
</div>
EOS
end
