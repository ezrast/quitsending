def note(text)
  <<EOS
  <span class="note_wrapper">
    <span class="empty"></span>
    <span class="note">
      <span>#{ text }</span>
    </span>
  </span>
EOS
end
